<?php

namespace Drupal\vallb;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\views\Element\View;
use Drupal\views\ViewEntityInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\ViewExecutableFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Views Ajax Lazy Load Blocks renderer.
 */
final class Renderer extends ControllerBase implements TrustedCallbackInterface {

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $viewStorage;

  /**
   * The view executable.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * Constructs the renderer instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\views\ViewExecutableFactory $viewExecutableFactory
   *   The view executable factory service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    protected ViewExecutableFactory $viewExecutableFactory,
  ) {
    $this->viewStorage = $entity_type_manager->getStorage('view');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->setViewExecutableFactory($container->get('views.executable'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['output'];
  }

  /**
   * Outputs the render array of a view block.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view id.
   * @param string $display_id
   *   The display id.
   * @param string $nojs
   *   The nojs parameter.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   The render array of the view.
   *
   * @throws \Drupal\views\Exception\ViewRenderElementException
   */
  public function output(ViewEntityInterface $view, string $display_id, string $nojs = 'nojs'): array|AjaxResponse {
    $query = \Drupal::requestStack()->getCurrentRequest()->query->all();
    $views_arguments = [];

    $view = $this->viewExecutable($view);
    $view_id = $view->id();
    if (!empty($query['vallb_views_arguments'])) {
      $views_arguments = $query['vallb_views_arguments'];
      $selector_hash_key = Crypt::hashBase64(serialize($views_arguments));
      $selector = "[data-vallb-container='$view_id:$display_id:$selector_hash_key'] [data-vallb-container-placeholder]";
      unset($query['vallb_views_arguments']);
    }
    else {
      $selector = "[data-vallb-container='$view_id:$display_id'] [data-vallb-container-placeholder]";
    }

    if ($query) {
      $view->setExposedInput($query);
    }
    $output = $view->buildRenderable($display_id, $views_arguments, FALSE);
    $this->addContextualLinks($output, $view);

    $output = View::preRenderViewElement($output);

    // When view_build is empty, the actual render array output for this View
    // is going to be empty. In that case, return just #cache, so that the
    // render system knows the reasons (cache contexts & tags) why this Views
    // block is empty, and can cache it accordingly.
    if (empty($output['view_build'])) {
      $output = ['#cache' => $output['#cache']];
    }

    if ($nojs == 'ajax') {
      $response = new AjaxResponse();
      $response->addCommand(new ReplaceCommand($selector, $output));
      return $response;
    }

    return $output;
  }

  /**
   * Checks the access control of the view.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view id or the view config entity.
   * @param string $display_id
   *   The view display id.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkOutputAccess(ViewEntityInterface $view, string $display_id): AccessResultInterface {
    $view = $this->viewExecutable($view);
    return $view->access($display_id) ? AccessResult::allowed() : AccessResult::forbidden();
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the view executable factory.
   *
   * @param \Drupal\views\ViewExecutableFactory $view_executable_factory
   *   The view executable factory service.
   */
  public function setViewExecutableFactory(ViewExecutableFactory $view_executable_factory): void {
    $this->viewExecutableFactory = $view_executable_factory;
  }

  /**
   * Adds the contextual links to the view output.
   *
   * @param array $output
   *   The output.
   * @param \Drupal\views\ViewExecutable $view
   *   The view executable.
   */
  protected function addContextualLinks(array &$output, ViewExecutable $view): void {
    if ($output) {
      $output['#view_id'] = $output['#name'];
      $output['#view_display_show_admin_links'] = $view->getShowAdminLinks();
      $output['#view_display_plugin_id'] = $view->display_handler->getPluginId();
      views_add_contextual_links($output, 'block', $output['#display_id']);
    }
  }

  /**
   * Initialize the view executable property.
   *
   * @param \Drupal\views\ViewEntityInterface $view
   *   The view id.
   *
   * @return \Drupal\views\ViewExecutable
   *   The executable.
   */
  protected function viewExecutable(ViewEntityInterface $view): ViewExecutable {
    if (!$this->view) {
      $this->view = $this->viewExecutableFactory->get($view);
    }
    return $this->view;
  }

}
