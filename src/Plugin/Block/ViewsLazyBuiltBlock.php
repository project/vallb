<?php

namespace Drupal\vallb\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\Block\ViewsBlock;

/**
 * Provides a views lazy built block.
 */
#[Block(
  id: 'vallb_block',
  admin_label: new TranslatableMarkup('Views Ajax Lazy Load Blocks'),
  deriver: 'Drupal\vallb\Plugin\Derivative\ViewsLazyBuiltBlock'
)]
class ViewsLazyBuiltBlock extends ViewsBlock {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $ajax_identifier = $this->view->id() . ':' . $this->displayID;
    $route_parameters = [
      'view' => $this->view->id(),
      'display_id' => $this->displayID,
      'nojs' => 'nojs',
    ];
    $options = [
      'query' => \Drupal::requestStack()->getCurrentRequest()->query->all(),
    ];
    $options['query'] += ['vallb_views_arguments' => []];

    return [
      '#theme' => 'vallb',
      '#ajax_identifier' => $ajax_identifier,
      '#route_parameters' => $route_parameters,
      '#route_options' => $options,
      '#route_name' => 'vallb.lazy_builder',
      '#view' => $this->view,
      '#view_display_id' => $this->displayID,
    ];
  }

}
