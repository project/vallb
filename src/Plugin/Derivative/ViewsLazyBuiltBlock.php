<?php

namespace Drupal\vallb\Plugin\Derivative;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\Derivative\ViewsBlock;

/**
 * Provides lazy load block plugin definitions for all Views block displays.
 *
 * @see \Drupal\views\Plugin\Block\ViewsBlock
 */
class ViewsLazyBuiltBlock extends ViewsBlock {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $derivatives = parent::getDerivativeDefinitions($base_plugin_definition);

    foreach ($derivatives as &$derivative) {
      // Change the category for blocks using this module.
      $derivative['category'] .= ' ' . new TranslatableMarkup('[Lazy Loaded]');
    }

    return $derivatives;
  }

}
