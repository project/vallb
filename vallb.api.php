<?php

/**
 * @file
 * Documentation of hooks related to the vallb module.
 */

use Drupal\Component\Utility\Crypt;
use Drupal\taxonomy\TermInterface;

/**
 * Preprocess theme variables for vallb theme hook.
 *
 * It allows module to inject views contextual arguments for example.
 *
 * @param array $variables
 *   The variables modified in place.
 */
function hook_preprocess_vallb(array &$variables) {
  // Example of adding a taxonomy id as a views argument.
  if ($taxonomy_term = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
    $variables['route_options']['query']['vallb_views_arguments'][] = $taxonomy_term instanceof TermInterface ? $taxonomy_term->id() : $taxonomy_term;
    $variables['ajax_identifier'] .= ':' . Crypt::hashBase64(serialize($variables['route_options']['query']['vallb_views_arguments']));
    $variables['attributes']['data-vallb-container'] = $variables['ajax_identifier'];
  }

  // Or preload existing libraries that might be needed by the lazy-loaded
  // block.
  $variables['#attached']['library'][] = 'charts_highstock/default';
  $variables['#attached']['library'][] = 'charts_highcharts/highcharts_exporting';
  $variables['#attached']['library'][] = 'charts_highmap/highmap';
  $variables['#attached']['library'][] = 'charts_highmap/charts_highmap';
  $variables['#attached']['library'][] = 'charts_overrides/charts_overrides';
}
