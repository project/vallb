# Views Ajax Lazy Builder Block

Build a view block output using intersection observer to make an ajax callback
to lazy load it.

This module adds a copy of your Views-generated blocks that can be placed into
your page instead of the Views-generated ones; these new blocks load via Ajax
(not the Drupal Core lazy-load methodology*) when the view content has been
rendered. This enables you to include a lot of data-intensive Views blocks on
your page without crashing your site when you land on an uncached page.

The use-case that spawned this module was a "dashboard" page with 20+
Views-generated blocks. Each block display had two attachments as tabs (see
https://www.drupal.org/project/views_attachment_tabs) and presented about 400
rows of data (per attachment) in a Chart (see
https://www.drupal.org/project/charts). Without this module, the initial visit
to this page would stall. Now, it loads rapidly.

* This module does not use Drupal Core's lazy-load feature because that feature
requires BigPipe to be enabled, and our hosting environment does not allow us
to use BigPipe.
