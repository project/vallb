/**
 * @file
 * Adds intersection observer to trigger ajax callback to lazily render the
 * views block.
 */

(function (Drupal, once) {
  /**
   * The intersection observer callback.
   *
   * @param payload
   */
  const intersectionCallback = (payload) => {
    payload.forEach(
      (containerIntObEntry) => {
        if (containerIntObEntry.isIntersecting) {
          Drupal.ajax({ url: containerIntObEntry.target.dataset.vallbEndpoint }).execute();
          intersection.unobserve(containerIntObEntry.target);
        }
      },
    );
  }

  const intersection = new IntersectionObserver(intersectionCallback);

  /**
   * Adds intersection observer to the views block placeholder.
   *
   * @type {Drupal.behavior}
   */
  Drupal.behaviors.vallb = {
    attach(context) {
      once('data-vallb-container-initialized', '[data-vallb-container]', context)
        .forEach(container => intersection.observe(container));
    },
  };
})(Drupal, once);
