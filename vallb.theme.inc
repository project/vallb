<?php

/**
 * @file
 * Theme functions for vallb.
 */

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Implements hook_preprocess_HOOK().
 */
function template_preprocess_vallb(array &$variables) {
  $variables['endpoint'] = Url::fromRoute($variables['route_name'], $variables['route_parameters'], $variables['route_options']);
  if (!empty($variables['route_options']['query']['vallb_views_arguments'])) {
    $variables['ajax_identifier'] .= ':' . Crypt::hashBase64(serialize($variables['route_options']['query']['vallb_views_arguments']));
  }
  $variables['attributes'] = new Attribute([
    'data-vallb-container' => $variables['ajax_identifier'],
    'data-vallb-endpoint' => $variables['endpoint']->toString(),
  ]);
  $variables['placeholder_attributes'] = new Attribute([
    'data-vallb-container-placeholder' => TRUE,
    'class' => [],
  ]);
}
